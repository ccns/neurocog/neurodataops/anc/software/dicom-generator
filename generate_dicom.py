import os
import argparse
import json
import numpy
from datetime import datetime
import pydicom
import pydicom._storage_sopclass_uids

# The starting point of this script was https://stackoverflow.com/a/69996257
parser = argparse.ArgumentParser(description='Generate a DICOM file that can be converted to NIfTI by dcm2niix.')
parser.add_argument(
    'dicom_config',
    metavar = 'DICOM_CONFIG',
    type = str,
    help = 'Path to config file with details of DICOM files to generate.'
)
parser.add_argument(
    'output_dir',
    metavar = 'OUTPUT_DIRECTORY',
    type = str,
    help = 'Path to output file.'
)
parser.add_argument(
    '--filename-template', '-t',
    dest = 'filename_template',
    metavar = 'FILENAME_TEMPLATE',
    type = str,
    default = "MR00{id}.dcm",
    help = 'Template for DICOM file names using Python string format syntax.'
)
args = parser.parse_args()

# Create a template DICOM dataset with constant values.
def generate_dicom(series_description, image_type, series_number, echo_numbers = None):
    # Metadata
    fileMeta = pydicom.Dataset()
    fileMeta.MediaStorageSOPClassUID = pydicom._storage_sopclass_uids.MRImageStorage
    fileMeta.TransferSyntaxUID = pydicom.uid.ExplicitVRLittleEndian
    fileMeta.MediaStorageSOPInstanceUID = pydicom.uid.generate_uid()
    # Create DICOM dataset
    ds = pydicom.Dataset()
    ds.file_meta = fileMeta
    # Values dcm2niix and BIDScoin  complained about not being set.
    ds.Manufacturer = 'SIEMENS'
    ds.InstanceNumber = '1'
    ds.Modality = 'MR'
    ds.AcquisitionDate = datetime.today().strftime('%Y%m%d')
    ds.AcquisitionTime = datetime.today().strftime('%H%M%S.%f')
    # User specified values
    ds.SeriesDescription = series_description
    ds.SeriesNumber = series_number
    # fmap magnitude images
    if image_type == 'magnitude' and echo_numbers is not None:
        ds.ImageType = ['ORIGINAL', 'PRIMARY', 'M', 'ND']
        ds.EchoNumbers = echo_numbers
        ds.EchoTime = echo_numbers + 5.2
    # fmap phase images
    if image_type == 'phase':
        ds.ImageType = ['ORIGINAL', 'PRIMARY', 'P', 'ND']
    # fmri images
    if image_type == 'fmri':
        ds.ImageType = ['ORIGINAL', 'PRIMARY', 'M', 'ND', 'MOSAIC']
    # anat images
    if image_type == 'anat':
        ds.ImageType = ['ORIGINAL', 'PRIMARY', 'M', 'ND']
    # Fixed values
    image = numpy.random.randint(2**16, size=(32, 32), dtype=numpy.uint16)
    ds.ImageOrientationPatient = [1, 0, 0, 0, 1, 0]
    ds.Rows = image.shape[0]
    ds.Columns = image.shape[1]
    ds.PixelSpacing = [1, 1] # in mm
    ds.SliceThickness = 1 # in mm
    ds.BitsAllocated = 16
    ds.PixelRepresentation = 1
    ds.PixelData = image.tobytes()
    return ds

# Load the config and iterate over all DICOMs to generate.
dicoms = list()
with open(args.dicom_config) as dicom_config:
    dicoms = json.load(dicom_config)

# Create ouptut dir if does not exist.
if not os.path.exists(args.output_dir):
    os.makedirs(args.output_dir)

# DICOM file id iterator
dicom_id = 1
series_number = 1
for d in dicoms:
    # Generate and save a single DICOM file or two for magnitude fieldmap.

    image_type = d['ImageType']
    series_description = d['SeriesDescription']
    
    if image_type == 'magnitude':
        dicom = generate_dicom(series_description, image_type, series_number, 1)
        file_path = os.path.join(args.output_dir, args.filename_template.format(id=dicom_id))
        dicom.save_as(file_path, write_like_original=False)
        dicom_id += 1
        dicom = generate_dicom(series_description, image_type, series_number, 2)
        file_path = os.path.join(args.output_dir, args.filename_template.format(id=dicom_id))
        dicom.save_as(file_path, write_like_original=False)
    else:
        dicom = generate_dicom(series_description, image_type, series_number)
        file_path = os.path.join(args.output_dir, args.filename_template.format(id=dicom_id))
        dicom.save_as(file_path, write_like_original=False)
    
    dicom_id += 1
    series_number += 1
