FROM debian:stable-slim

WORKDIR /opt

# Install dependencies
RUN apt-get -y update \
 && apt-get -y install git python3-pip \
 && /usr/bin/python3 -m pip install --upgrade pip \
 && git clone https://gitlab.com/ccns/neurocog/neurodataops/anc/software/dicom-generator.git \
 && cd dicom-generator \
 && git checkout 2-fix-dockerfile \
 && pip3 install -r requirements.txt

ENTRYPOINT ["/usr/bin/python3", "/opt/dicom-generator/generate_dicom.py"]

