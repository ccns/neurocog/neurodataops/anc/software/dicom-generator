# DICOM Generator

`generate_dicom.py` generates a list of dummy DICOM files for testing purposes.

The starting point of this script was https://stackoverflow.com/a/69996257

The list of DICOM files is specified in a configuration file. An example file looks like this:

```json
[
  {
    "ImageType" : "anat",
    "SeriesDescription" : "T1w"
  },
  {
    "ImageType" : "magnitude",
    "SeriesDescription" : "fmap"
  },
  {
    "ImageType" : "phase",
    "SeriesDescription" : "fmap"
  },
  {
    "ImageType" : "fmri",
    "SeriesDescription" : "fmri"
  }
]
```

There is no JSON schema validation at the moment. Each image object must have the following keys:

- `ImageType` represents the generated image. Depending on its value, DICOM `ImageType` value is assigned. The value is one of:
   - `anat`
   - `magnitude` - two magnitude DICOM files are generated with two different `EchoNumbers` and `EchoTime`
   - `phase`
   - `fmri`
- `SeriesDescription` is any string, better with no spaces and special characters. There is no validation.

## Usage information

```
python3 generate_dicom.py --help
```
or using Docker from the project's container registry (mount the volumes if necessary):
```
sudo docker run --rm -it registry.gitlab.com/ccns/neurocog/neurodataops/anc/software/dicom-generator -h
```

## Dependencies

- [pydicom](https://pydicom.github.io/)
- numpy

Use [Python virtual environment](https://docs.python.org/3/tutorial/venv.html) and install dependencies.
```bash
python3 -m venv .venv
source .venv/bin/activate
pip3 install -U -r requirements.txt
```

